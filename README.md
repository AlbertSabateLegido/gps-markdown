# Exemple de Markdown

Aquest és un exemple de document Markdown amb imatges i altres documents locals. Mireu el codi font de README.md per a veure com crear documents com aquest.

- Podeu fer un fork o un clon del repositori amb [GIT](http://git-scm.com)
- Podeu descarregar el codi font des de la [zona de descàrregues de Bitbucket](https://bitbucket.org/jordipradel/gps-markdown/downloads)

Es poden fer títols afegint entre 1 i 8 símbols # a l'inici de la línia:

- 1 sol #: Títol de primer nivell
- 2 #'s: Títol de segon nivell

Dues línies amb només un salt de línia entre elles
formen un sol paràgraf.

Per separar paràgrafs cap posar dos salts de línia.

Es poden posar notes al peu de pàgina com aquesta [^nota] o aquesta altra [^altra].

[^nota]: **Compte!** El nom de la nota no apareix al document. Les notes es numeren automàticament.

[^altra]: La nota es llegeix al final del document, però es pot escriure prop d'on apareix al text.

## Format tipogràfic

- Es poden fer negretes **així** o __així__. 
- Les cursives es fan *així* o _així_.
- Es poden fer *__totes dues coses__* alhora.
- Es pot mostrar ~~text tatxat~~

## Enllaços
 
- [Sintaxi Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Enllaç relatiu](./fonts/markdown.md)

## Llistes

Llistes numerades:

1. Primer
    - Primer a
    - Primer b 
1. Segon

    Es pot posar un paràgraf aliniat dins una llista indentant-lo amb espais.

1. Tercer

Llistes sense numerar

- Primer
- Segon
- Tercer
    - Tercer a
    - Tercer b
- Quart

## Blocs

    Un paràgraf que comença amb 4 espais o un tabulador es mostra com un bloc.
    En un bloc, els salts de línia es repsecten tal qual s'escriguin.

> Un paràgraf amb cràcters '>' a l'inici de cada línia es mostra com una cita.
> 
> Els salts de línia dins una cita funcionen com a la resta de Markdown (cal posar-ne dos per provocar un salt de línia).
> > Es poden posar diversos nivells de cita


## Imatges

### Imatges públiques a Internet

![Markdown logo](https://cdn.tutsplus.com/net/uploads/legacy/2063_markdown/preview.png "Markdown logo")

### Imatges locals

![The Private Investocat](./images/privateinvestocat.jpg "The Private Investocat")
[^notaImatgeBitbucket]

[^notaImatgeBitbucket]: La imatge no es visualitza correctament a Bitbucket, però es visualitza correctament amb un visualitzador si teniu una còpia local de tota el document (incloent la subcarpeta images).

## Taules

| Opció  | Descripció |
| ------ | ---------- |
| Taules | No estàndard. Només funcionen amb alguns visualitzadors. |

## Codi

Es pot indicar codi en mig d'un paràgraf, com ara `n+1`, o com a bloc:

```
function sum(a,b){
  resturn a + b;
}
```

## Eines

- No conec les de windows, però he trobat [MarkdownPad](http://markdownpad.com).
- En Mac jo faig servir [MacDown](http://macdown.uranusjr.com)

